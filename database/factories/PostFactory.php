<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        try {
            return $this->afterCreating(function (Post $post) {
                $post->addMediaFromUrl($this->faker->imageUrl(1024, 768))
                    ->usingFileName(rand() . '.jpg')
                    ->toMediaCollection('image');
            });
        } catch (\Exception $e) {
            return $this;
        }
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->realTextBetween(40, 100),
        ];
    }
}
