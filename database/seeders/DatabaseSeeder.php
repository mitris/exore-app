<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Schema::disableForeignKeyConstraints();
        foreach (['media', 'posts', 'model_has_roles', 'roles', 'users', 'categories'] as $table) {
            DB::table($table)->truncate();
        }
        Schema::enableForeignKeyConstraints();
        Model::reguard();

        $categories = Category::factory(20)->create();

        $managerRole = Role::create(['name' => 'manager']);
        $employeeRole = Role::create(['name' => 'employee']);

        User::factory()
            ->count(2)
            ->hasAttached($managerRole)
            ->has(
                User::factory()
                    ->count(3)
                    ->hasAttached($employeeRole)
                    ->hasPosts(5, fn() => ['category_id' => $categories->random()->id]),
                'employees'
            )
            ->create();

        if ($adminUser = User::find(1)) {
            $adminUser->update(['email' => 'manager@site.com']);
        }
    }
}
