@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'employees') }}
        <div class="card">
            <h5 class="card-header">Employee</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        {{ aire()->input('name', 'Name')}}
                        {{ aire()->input('email', 'Email')}}
                        {{ aire()->password('password', 'Password')->value('') }}
                        {{ aire()->password('password_confirmation', 'Password confirmation')->value('') }}
                    </div>
                </div>
            </div>
            <div class="card-footer pt-3 pb-3">
                <button class="btn btn-primary">{{ $model->exists ? 'Save' : 'Create' }}</button>
                <a href="{{ route('employees.index') }}" class="btn btn-outline-secondary">Back to list</a>
            </div>
        </div>
    {{ aire()->close() }}
@endsection
