@extends('layouts.app')

@section('content')
    <div class="mb-3">
        <a href="{{ route('employees.create') }}" class="btn btn-secondary">Create New Employee</a>
    </div>
    <div class="card">
        <div class="card-header">
            Employees
        </div>
        @if($results->total())
            <table class="table table-striped table-hover mb-0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($results as $item)
                    <tr>
                        <td>
                            <a href="{{ route('employees.edit', $item) }}">{{ $item->name }}</a>
                        </td>
                        <td>
                            <small>{{ $item->email }}</small>
                        </td>
                        <td>
                            <div class="text-nowrap text-end">
                                <a href="{{ route('employees.edit', $item) }}" class="btn btn-sm btn-primary">Edit</a>
                                <a href="{{ route('employees.destroy', $item) }}" class="btn btn-sm btn-danger" data-action="destroy">Delete</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($results->hasPages())
                <div class="card-footer clearfix">
                    {{ $results->withQueryString()->links() }}
                </div>
            @endif
        @else
            <div class="card-body">
                <div class="alert alert-danger mb-0">Nothing to show</div>
            </div>
        @endif
    </div>
@endsection

