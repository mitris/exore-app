<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <x-css />
    </head>
    <body>
        <x-nav />
        <main class="container-fluid py-4">
            <x-alert-message/>
            @yield('content')
        </main>
        <x-js />
    </body>
</html>
