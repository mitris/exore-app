@extends('layouts.app')

@section('content')
    <div class="card mb-3">
        <div class="card-body pb-0">
            {{ aire()->open()->route('posts.index')->bind(['filter' => request('filter')]) }}
                <div class="row">
                    <div class="col-3">
                        {{ aire()->select($categories, 'filter[category_id]')->prependEmptyOption('- Select Category -') }}
                    </div>
                    @hasrole('manager')
                        <div class="col-3">
                            {{ aire()->select($employees, 'filter[user_id]')->prependEmptyOption('- Select Employee -') }}
                        </div>
                    @endif
                    <div class="col">
                        <button class="btn btn-secondary">Filter</button>
                        @if(request()->has('filter'))
                            <a href="{{ route('posts.index') }}" class="btn btn-primary">Reset Filter</a>
                        @endif
                        @can('create', \App\Models\Post::class)
                            <a href="{{ route('posts.create') }}" class="btn btn-secondary float-end">Create New Post</a>
                        @endcan
                    </div>
                </div>
            {{ aire()->close() }}
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            Posts
        </div>
        @if($results->total())
            <table class="table table-striped table-hover mb-0 align-middle">
                <thead>
                    <tr>
                        <th>Title</th>
                        @if(auth()->user()->hasRole('manager'))
                            <th>Employee</th>
                        @endif
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $item)
                        <tr>
                            <td>
                                @if($image = $item->getFirstMediaUrl('image', 'xs'))
                                    <a href="{{ $item->getFirstMediaUrl('image', 'md') }}" class="strip float-end">
                                        <img src="{{ $item->getFirstMediaUrl('image', 'xs') }}" alt="{{ $item->title }}" class="img-thumbnail">
                                    </a>
                                @endif
                                <a href="{{ route('posts.edit', $item) }}"><b>{{ $item->title }}</b></a>
                                @if($item->category->exists)
                                    <div>
                                        <small>Category: <a href="{{ route('posts.index', ['filter' => ['category_id' => $item->category->id] + request('filter', [])]) }}">{{ $item->category->name }}</a></small>
                                    </div>
                                @endif
                                <div>
                                    <small class="text-muted">{{ $item->created_at->toDateTimeString() }}</small>
                                </div>
                            </td>
                            @hasrole('manager')
                                <td>
                                    <a href="{{ route('posts.index', ['filter' => ['user_id' => $item->user->id] + request('filter', [])]) }}">{{ $item->user->name }}</a>
                                    <br>
                                    <small class="text-muted">{{ $item->user->email }}</small>
                                </td>
                            @endhasrole
                            <td>
                                <div class="text-nowrap text-end">
                                    @if($item->user_id == auth()->id())
                                        <a href="{{ route('posts.edit', $item) }}" class="btn btn-sm btn-primary">Edit</a>
                                    @endif
                                    <a href="{{ route('posts.destroy', ['model' => $item] + request()->all()) }}" class="btn btn-sm btn-danger" data-action="destroy">Delete</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            @if($results->hasPages())
                <div class="card-footer clearfix">
                    {{ $results->withQueryString()->links() }}
                </div>
            @endif
        @else
            <div class="card-body">
                <div class="alert alert-danger mb-0">Nothing to show</div>
            </div>
        @endif
    </div>
@endsection

