@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'posts')->encType('multipart/form-data') }}
        <div class="card">
            <h5 class="card-header">Post</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        {{ aire()->input('title', 'Title')}}
                    </div>
                    <div class="col-6">
                        {{ aire()->select($categories, 'category_id', 'Category')}}
                        {{ aire()->file('image', 'Image') }}
                        @if($model->exists)
                            @if($image = $model->getFirstMediaUrl('image', 'sm'))
                                <div class="mt-3">
                                    <a href="{{ $model->getFirstMediaUrl('image', 'md') }}" class="strip">
                                        <img src="{{ $image }}" alt="{{ $model->title }}" class="img-thumbnail">
                                    </a>
                                </div>
                                <a href="{{ route('posts.remove_image', $model) }}" data-action="post-with-confirm"><small>remove image</small></a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer pt-3 pb-3">
                <button class="btn btn-primary">{{ $model->exists ? 'Save' : 'Create' }}</button>
                <a href="{{ route('posts.index') }}" class="btn btn-outline-secondary">Back to list</a>
            </div>
        </div>
    {{ aire()->close() }}
@endsection
