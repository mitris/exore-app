@if(session('success') || session('error') || session('danger') || session('warning') || $errors->any())
    <div class="mb-3">
        @if (session('success'))
            <div class="alert alert-success mb-0">
                {!! session('success') !!}
            </div>
        @endif

        @if (session('error') || session('danger'))
            <div class="alert alert-danger mb-0">
                {!! session('error') . ' ' . session('danger') !!}
            </div>
        @endif

        @if (session('warning'))
            <div class="alert alert-warning mb-0">
                {!! session('warning') !!}
            </div>
        @endif

        @if($errors->any())
            <div class="alert alert-danger mb-0">
                @foreach($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif
    </div>
@endif
