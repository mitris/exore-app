<form method="post" data-action="destroy-form">
    @csrf
    @method('DELETE')
</form>

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@staaky/strip@1.8.0/dist/js/strip.pkgd.min.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endpush

@stack('js')
