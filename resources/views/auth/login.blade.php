@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header text-center">{{ __('Login') }}</div>

                <div class="card-body">
                    {{ aire()->open()->route('login') }}
                        {{ aire()->input('email', 'Email address') }}
                        {{ aire()->password('password', 'Password') }}

                        <div class="row justify-content-center">
                            <div class="col">
                                {{ aire()->checkbox('remember', 'Remember me') }}
                            </div>
                            <div class="col text-end">
                                <button type="submit" class="btn btn-primary">{{ __('Login') }}</button>
                            </div>
                        </div>
                    {{ aire()->close() }}
                    <div class="row justify-content-md-center">
                        <div class="col-auto">
                            <div class="alert alert-warning mb-0 mt-5 text-center px-4">
                                <small>
                                    Default access credentials:
                                    <br>
                                    Email: <b>manager@site.com</b>
                                    <br>
                                    Password: <b>secret</b>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
