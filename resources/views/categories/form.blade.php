@extends('layouts.app')

@section('content')
    {{ aire()->resourceful($model, 'categories') }}
        <div class="card">
            <h5 class="card-header">Category</h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        {{ aire()->input('name', 'Name')}}
                    </div>
                </div>
            </div>
            <div class="card-footer pt-3 pb-3">
                <button class="btn btn-primary">{{ $model->exists ? 'Save' : 'Create' }}</button>
                <a href="{{ route('categories.index') }}" class="btn btn-outline-secondary">Back to list</a>
            </div>
        </div>
    {{ aire()->close() }}
@endsection
