@extends('layouts.app')

@section('content')
    @can('create', \App\Models\Category::class)
        <div class="mb-3">
            <a href="{{ route('categories.create') }}" class="btn btn-secondary">Create New Category</a>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            Categories
        </div>
        @if($results)
            <table class="table table-striped table-hover mb-0">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>{{ auth()->user()->hasRole('manager') ? 'Posts by your employees' : 'Your posts' }}</th>
                        @can(['update', 'delete'], \App\Models\Category::class)
                            <th></th>
                        @endcan
                    </tr>
                </thead>
                <tbody>
                @foreach($results as $item)
                    <tr>
                        <td>
                            <a href="{{ route('posts.index', ['filter' => ['category_id' => $item->id]]) }}">{{ $item->name }}</a>
                        </td>
                        <td>
                            <a href="{{ route('posts.index', ['filter' => ['category_id' => $item->id]]) }}" title="Post count" data-toggle="tooltip"><small class="badge bg-secondary">{{ $item->posts_count }}</small></a>
                        </td>
                        @can(['update', 'delete'], \App\Models\Category::class)
                            <td>
                                <div class="text-nowrap text-end">
                                    @can('update', \App\Models\Category::class)
                                        <a href="{{ route('categories.edit', $item) }}" class="btn btn-sm btn-primary">Edit</a>
                                    @endcan
                                    @can('delete', \App\Models\Category::class)
                                        <a href="{{ route('categories.destroy', $item) }}" class="btn btn-sm btn-danger" data-action="destroy">Delete</a>
                                    @endcan
                                </div>
                            </td>
                        @endcan
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="card-body">
                <div class="alert alert-danger mb-0">Nothing to show</div>
            </div>
        @endif
    </div>
@endsection

