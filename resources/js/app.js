$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document)
        .on('click', '[data-action="destroy"]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure?')) {
                $('[data-action="destroy-form"]').attr('action', $(this).attr('href')).submit();
            }
        })
        .on('click', '[data-action="post"]', function (e) {
            e.preventDefault();
            let form = $('[data-action="destroy-form"]');
            form.find('[name="_method"]').remove();
            form.attr('action', $(this).attr('href')).submit();
        })
        .on('click', '[data-action="post-with-confirm"]', function (e) {
            e.preventDefault();
            if (confirm('Are you sure?')) {
                let form = $('[data-action="destroy-form"]');
                form.find('[name="_method"]').remove();
                form.attr('action', $(this).attr('href')).submit();
            }
        });

    $('[data-toggle="tooltip"]').tooltip();
});
