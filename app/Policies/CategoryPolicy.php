<?php

namespace App\Policies;

use App\Models\Category;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->hasRole('manager');
    }

    public function update(User $user)
    {
        return $user->hasRole('manager');
    }

    public function delete(User $user)
    {
        return $user->hasRole('manager');
    }
}
