<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->hasRole('employee');
    }

    public function update(User $user, Post $model)
    {
        return $user->id === $model->user_id;
    }

    public function delete(User $user, Post $model)
    {
        return $user->id === $model->user_id || ($user->hasRole('manager') && $user->id === optional($model->user)->manager_id);
    }
}
