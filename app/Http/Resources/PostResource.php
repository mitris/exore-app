<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category' => optional($this->category)->toArray(),
            'user' => $this->user,
            'image' => $this->when($this->getFirstMediaUrl('image'), function () {
                return [
                    'original' => $this->getFirstMediaUrl('image'),
                    'xm' => $this->getFirstMediaUrl('image', 'xs'),
                    'sm' => $this->getFirstMediaUrl('image', 'sm'),
                    'md' => $this->getFirstMediaUrl('image', 'md'),
                ];
            }),
            'created_at' => $this->created_at,
        ];
    }
}
