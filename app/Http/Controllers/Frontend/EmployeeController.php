<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query()
            ->orderByDesc('created_at')
            ->employees($request->user()->id);

        return view('employees.index', ['results' => $query->paginate(config('settings.max_per_page'))]);
    }

    public function create()
    {
        return view('employees.form', ['model' => new User()]);
    }

    public function store(StoreUserRequest $request)
    {
        User::create($request->validated() + ['manager_id' => $request->user()->id]);

        return redirect()->route('employees.index')->with('success', 'Employee created');
    }

    public function edit(User $model)
    {
        return view('employees.form', ['model' => $model]);
    }

    public function update(UpdateUserRequest $request, User $model)
    {
        $model->name = $request->validated('name');
        $model->email = $request->validated('email');
        if ($request->validated('password')) {
            $model->password = \Hash::make($request->validated('password'));
        }
        $model->save();

        return redirect()->route('employees.index')->with('success', 'Employee updated');
    }

    public function destroy(User $model)
    {
        $model->delete();

        return redirect()->route('employees.index')->with('success', 'Employee deleted');
    }
}
