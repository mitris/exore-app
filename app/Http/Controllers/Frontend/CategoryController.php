<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'model');
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $query = Category::query()
            ->withCount(['posts' => function (Builder $query) use ($user) {
                if ($user->hasRole('manager')) {
                    $query->whereIn('user_id', $user->employees->pluck('id'));
                } else {
                    $query->where('user_id', $user->id);
                }
            }])
            ->orderBy('name');

        return view('categories.index', ['results' => $query->get()]);
    }

    public function create()
    {
        return view('categories.form', ['model' => new Category()]);
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->validated());

        return redirect()->route('categories.index')->with('success', 'Category created');
    }

    public function edit(Category $model)
    {
        return view('categories.form', ['model' => $model]);
    }

    public function update(CategoryRequest $request, Category $model)
    {
        $model->update($request->validated());

        return redirect()->route('categories.index')->with('success', 'Category updated');
    }

    public function destroy(Category $model)
    {
        $model->delete();

        return redirect()->route('categories.index')->with('success', 'Category deleted');
    }
}
