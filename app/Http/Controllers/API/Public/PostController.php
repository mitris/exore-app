<?php

namespace App\Http\Controllers\API\Public;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use App\Models\Post;

class PostController extends Controller
{
    public function index()
    {
        $query = Post::query()
            ->orderByDesc('created_at')
            ->with(['user', 'category', 'media']);

        return PostResource::collection($query->paginate(config('settings.max_per_page')));
    }

    public function show(Post $model)
    {
        $model->load(['user', 'category', 'media']);

        return new PostResource($model);
    }
}
