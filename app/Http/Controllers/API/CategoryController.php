<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Category::class, 'model');
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $query = Category::query()
            ->withCount(['posts' => function (Builder $query) use ($user) {
                if ($user->hasRole('manager')) {
                    $query->whereIn('user_id', $user->employees->pluck('id'));
                } else {
                    $query->where('user_id', $user->id);
                }
            }])
            ->orderBy('name');

        return new JsonResource(['results' => $query->get()]);
    }

    public function store(CategoryRequest $request)
    {
        $model = Category::create($request->validated());

        return new JsonResource($model);
    }

    public function update(CategoryRequest $request, Category $model)
    {
        $model->update($request->validated());

        return new JsonResource($model);
    }

    public function destroy(Category $model)
    {
        $model->delete();


    }
}
