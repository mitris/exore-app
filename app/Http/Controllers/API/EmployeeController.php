<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query()
            ->orderByDesc('created_at')
            ->employees($request->user()->id);

        return new JsonResource(['results' => $query->paginate(config('settings.max_per_page'))]);
    }

    public function store(StoreUserRequest $request)
    {
        $model = User::create($request->validated() + ['manager_id' => $request->user()->id]);

        return new JsonResource($model);
    }

    public function update(UpdateUserRequest $request, User $model)
    {
        $model->name = $request->validated('name');
        $model->email = $request->validated('email');
        if ($request->validated('password')) {
            $model->password = \Hash::make($request->validated('password'));
        }
        $model->save();

        return new JsonResource($model);
    }

    public function destroy(User $model)
    {
        $model->delete();

        return new JsonResource(['success' => true]);
    }
}
