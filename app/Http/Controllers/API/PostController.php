<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Post::class, 'model');
    }

    public function index(Request $request)
    {
        $query = Post::query()
            ->with(['user', 'category'])
            ->orderByDesc('created_at')
            ->category($request->input('filter.category_id'));

        $user = $request->user();

        if ($user->hasRole('manager')) {
            if ($userId = $request->input('filter.user_id')) {
                $query->byUser($userId);
            } else {
                $query->byEmployees($user->id);
            }
        } else {
            $query->byUser($user->id);
        }

        return new JsonResource([
            'results' => $query->paginate(config('settings.max_per_page')),
            'categories' => Category::orderBy('name')->get()->pluck('name', 'id'),
            'employees' => $user->hasRole('manager') ? User::getEmployees($user->id)->pluck('name', 'id') : [],
        ]);
    }

    public function store(StorePostRequest $request)
    {
        $model = new Post($request->validated());
        $model->user_id = $request->user()->id;
        $model->save();

        if ($request->hasFile('image')) {
            $model->addMediaFromRequest('image')
                ->usingFileName(rand() . '.jpg')
                ->toMediaCollection('image');
        }

        return new JsonResource($model->load('category'));
    }

    public function show(Post $model)
    {
        return new JsonResource($model->load('category'));
    }

    public function update(Request $request, Post $model)
    {
        $model->update($request->validated());

        if ($request->hasFile('image')) {
            $model->addMediaFromRequest('image')
                ->usingFileName(rand() . '.jpg')
                ->toMediaCollection('image');
        }

        return new JsonResource($model->load('category'));
    }

    public function destroy(Post $model)
    {
        $model->delete();

        return new JsonResource(['success' => true]);
    }

    public function removeImage(Post $model)
    {
        $this->authorize('update', $model);

        if ($image = $model->getFirstMedia('image')) {
            $image->delete();
        }

        return new JsonResource(['success' => true]);
    }
}
