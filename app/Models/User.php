<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    protected $fillable = [
        'name',
        'email',
        'password',
        'manager_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $visible = [
        'id',
        'name',
        'email',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function employees()
    {
        return $this->hasMany(self::class, 'manager_id', 'id');
    }

    public function manager()
    {
        return $this->belongsTo(self::class, 'manager_id', 'id')->withDefault();
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getEmployeesPosts()
    {
        // implement
    }

    public function scopeEmployees($query, $manager_id)
    {
        $query->whereManagerId($manager_id);
    }

    public static function getEmployees($manager_id)
    {
        return User::where('manager_id', $manager_id)->get();
    }
}
