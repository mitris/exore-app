<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Post extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $fillable = [
        'category_id',
        'title',
    ];

    protected $hidden = [
        'user_id',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')
            ->acceptsMimeTypes([
                'image/jpeg',
                'image/png',
                'image/svg',
                'image/svg+xml',
            ])
            ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('xs')
            ->crop(Manipulations::CROP_CENTER, 50, 50)
            ->performOnCollections('image')
            ->nonQueued();

        $this->addMediaConversion('sm')
            ->crop(Manipulations::CROP_CENTER, 240, 150)
            ->performOnCollections('image')
            ->nonQueued();

        $this->addMediaConversion('md')
            ->fit(Manipulations::FIT_CONTAIN, 900, 900)
            ->performOnCollections('image')
            ->nonQueued();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault();
    }

    public function scopeCategory($query, $category_id = null)
    {
        if ($category_id !== null) {
            $query->where('category_id', $category_id);
        }
    }

    public function scopeByEmployees($query, $manager_id)
    {
        $query->whereIn('user_id', User::getEmployees($manager_id)->pluck('id'));
    }

    public function scopeByUser($query, $user_id)
    {
        $query->where('user_id', $user_id);
    }
}
