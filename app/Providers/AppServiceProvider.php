<?php

namespace App\Providers;

use Faker\{Factory, Generator};
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Smknstd\FakerPicsumImages\FakerPicsumImagesProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Generator::class, function ($app) {
            $faker = Factory::create();
            $faker->addProvider(new FakerPicsumImagesProvider($faker));

            return $faker;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrapFive();
        JsonResource::withoutWrapping();
    }
}
