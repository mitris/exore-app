<?php

use App\Http\Controllers\Frontend\CategoryController;
use App\Http\Controllers\Frontend\EmployeeController;
use App\Http\Controllers\Frontend\PostController;
use Illuminate\Support\Facades\Route;

Auth::routes([
    'reset' => false,
    'confirm' => false,
]);

Route::middleware(['auth:web', 'role:manager|employee'])->group(function () {
    Route::get('/', [PostController::class, 'index'])->name('main');
    Route::post('/posts/{model}/remove-image', [PostController::class, 'removeImage'])->name('posts.remove_image');
    Route::resource('/posts', PostController::class)->except('show')->parameter('posts', 'model');

    Route::resource('/categories', CategoryController::class)->except('show')->parameter('categories', 'model');

    Route::middleware('role:manager')->group(function () {
        Route::resource('/employees', EmployeeController::class)->except('show')->parameter('employees', 'model');
    });
});
