<?php

use App\Http\Controllers\API\ {
    AuthController,
    PostController,
    CategoryController,
    EmployeeController,
    RegisterController
};
use App\Http\Controllers\API\Public\ {
    PostController as PublicPostController
};
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'auth', 'as' => 'api.'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout')->middleware('auth:sanctum');
    Route::post('register', [RegisterController::class, 'store'])->name('register')->middleware('guest');
});

Route::group(['prefix' => '_public', 'as' => 'api.public.'], function () {
    Route::group(['prefix' => 'posts', 'controller' => PublicPostController::class, 'as' => 'posts.'], function () {
        Route::get('/', 'index')->name('index');
        Route::get('/{model}', 'show')->name('show');
    });
});

Route::middleware('auth:sanctum')->name('api.')->group(function () {
    Route::post('/posts/{model}/remove-image', [PostController::class, 'removeImage'])->name('posts.remove_image');
    Route::apiResource('posts', PostController::class)->parameter('posts', 'model');

    Route::apiResource('categories', CategoryController::class)->except('show')->parameter('categories', 'model');

    Route::middleware('role:manager')->group(function () {
        Route::apiResource('employees', EmployeeController::class)->except('show')->parameter('employees', 'model');
    });
});

